package univers7.proto_univers7;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Proto_univers7 {

    public static void main(String[] args) {
        SpringApplication.run(Proto_univers7.class, args);
    }
}
