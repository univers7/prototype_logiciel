package univers7.proto_univers7.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.stereotype.Controller;
import univers7.proto_univers7.domain.Message;
import univers7.proto_univers7.services.MessageService;


@Controller
public class MessageController {
    
    private MessageService messageService;

    @Autowired
    public void setMessageService(MessageService messageService) {
        this.messageService = messageService;
    }

    @RequestMapping(value = "/messages", method = RequestMethod.GET)
    public String list(Model model){
        model.addAttribute("messages", messageService.listAllProducts());
        System.out.println("Returning rmessages:");
        return "messages";
    }

    @RequestMapping("message/{id}")
    public String showMessage(@PathVariable Integer id, Model model){
        model.addAttribute("message", messageService.getProductById(id));
        return "messageshow";
    }

    @RequestMapping("message/edit/{id}")
    public String edit(@PathVariable Integer id, Model model){
        model.addAttribute("message", messageService.getProductById(id));
        return "messageform";
    }

    @RequestMapping("message/new")
    public String newMessage(Model model){
        model.addAttribute("message", new Message());
        return "messageform";
    }

    @RequestMapping(value = "message", method = RequestMethod.POST)
    public String saveMessage(Message message){

        messageService.saveProduct(message);

        return "redirect:/message/" + message.getId();
    }
}
