package univers7.proto_univers7.repositories;

import org.springframework.data.repository.CrudRepository;
import univers7.proto_univers7.domain.Message;

public interface MessageRepository extends CrudRepository<Message, Integer>{
}
