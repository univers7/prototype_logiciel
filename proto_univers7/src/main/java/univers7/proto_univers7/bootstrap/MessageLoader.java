package univers7.proto_univers7.bootstrap;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import univers7.proto_univers7.domain.Message;
import univers7.proto_univers7.repositories.MessageRepository;

@Component
public class MessageLoader implements ApplicationListener<ContextRefreshedEvent>{
    
    private MessageRepository messageRepository;

    private Logger log = Logger.getLogger(MessageLoader.class);

    @Autowired
    public void setProductRepository(MessageRepository messageRepository) {
        this.messageRepository = messageRepository;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {

        Message message2 = new Message();
        message2.setId(2);
        message2.setMots("message test");
        messageRepository.save(message2);

        log.info("Saved message - id: " + message2.getId());
    }
    
}
