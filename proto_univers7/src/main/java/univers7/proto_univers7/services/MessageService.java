package univers7.proto_univers7.services;

import univers7.proto_univers7.domain.Message;

public interface MessageService {
    
    Iterable<Message> listAllProducts();

    Message getProductById(Integer id);

    Message saveProduct(Message message);
    
}
