package univers7.proto_univers7.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import univers7.proto_univers7.repositories.MessageRepository;
import univers7.proto_univers7.domain.Message;

@Service
public class MessageServiceImpl implements MessageService{
    
    private MessageRepository messageRepository;

    @Autowired
    public void setProductRepository(MessageRepository messageRepository) {
        this.messageRepository = messageRepository;
    }

    @Override
    public Iterable<Message> listAllProducts() {
        return messageRepository.findAll();
    }

    @Override
    public Message getProductById(Integer id) {
        return messageRepository.findOne(id);
    }

    @Override
    public Message saveProduct(Message message) {
        return messageRepository.save(message);
    }
    
}
